#include <avr/io.h>

#define led_pin (1 << PB2)

int main(void) {
    // Configure led_pin as an output.
    DDRB |= led_pin;

    // Set led_pin high.
    PORTB |= led_pin;

    // Nothing left to do, so just spin.
    while (1) {}

    return 0;
}

