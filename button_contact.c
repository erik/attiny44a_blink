#include <avr/io.h>

#define led_pin (1 << PB2)
#define button_pin (1 << PA7)

int main(void) {
    // Configure led_pin as an output.
    DDRB |= led_pin;

    // Configure button_pin as an input.
    DDRA &= ~button_pin;

    // Activate button_pin's pullup resistor.
    PORTA |= button_pin;

    while (1) {
        // Turn on the LED when the button is pressed.
        if (PINA & button_pin) {
            // Turn off the LED.
            PORTB &= ~led_pin;
        } else {
            PORTB |= led_pin;
        }
    }

    return 0;
}

