# ATTINY44A_blink

This repo contains a variety of simple programs for an ATTINY44A.

The majority are simple "hello world" type programs that only assume connection to
an LED and a button. Not coincidentally this is the sort of board I
ended up with after modifying Neil's
[echo board](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo)
based on the guidelines of the
[electronics design](http://academy.cba.mit.edu/classes/electronics_design/index.html)
assignment. Others involve bit-banging a serial connection to a computer using an FTDI
friend.

Read the comments in the Makefile for instructions on building and using this
code.
