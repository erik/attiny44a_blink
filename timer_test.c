#include <avr/io.h>

#define led_pin (1 << PB2)

int main(void) {
    // Set the clock prescaler to 1.
    CLKPR = (1 << CLKPCE);
    CLKPR = (0 << CLKPS3) | (0 << CLKPS2) | (0 << CLKPS1) | (0 << CLKPS0);

    // Set the 8 bit timer's prescaler to 1/1024.
    TCCR0B |= 0b00000101;

    // Configure led_pin as an output.
    DDRB |= led_pin;

    // Blink the LED with ~2.5s half-period.
    // 250 * 200 * 1024 / 20M ~ 2.5s
    int count = 0;
    while (1) {
        // If it's been 10ms, reset the timer and increment count.
        if (TCNT0 >= 200) {
            TCNT0 = 0;
            // When count reaches 250, reset it and toggle the LED.
            if (++count == 250) {
                PORTB ^= led_pin;
                count = 0;
            }
        }
    }

    return 0;
}

